<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResetPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', PasswordType::class, array(
                'mapped' => false,
                'label' => ' ',
                'attr' => array(
                    'class' => 'input100',
                    'type' => "password",
                    'placeholder' => "Ancien Mot de passe"
                ),
            ))
            ->add('password', RepeatedType::class, array(
                'first_options'  => [
                    'label' => ' ',
                    'attr'=> array(
                        'placeholder' => "Nouveau Mot de passe",
                        'class' => 'input100',
                        'type' => "password",
                    )],
                'second_options'  => ['label' => ' ',
                    'attr'=> array(
                        'placeholder' => "Retapez Mot de passe",
                        'class' => 'input100',
                        'type' => "password",
                    )],
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de passe doivent être identiques',
                'required' => true,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
