<?php

namespace App\Controller;

use App\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EntrepriseController extends AbstractController
{
    /**
     * Matches /entreprise exactly
     *
     * @Route("/entreprise", name="entreprise")
     */
    public function index()
    {
        $entreprises = $this->getDoctrine()->getRepository(Entreprise::class)->findAll();

        return $this->render('entreprise/index.html.twig', [
            'controller_name' => "",
            'entreprises' => $entreprises
        ]);
    }

    /**
     * Matches /entreprise/*
     *
     * @Route("/entreprise/{id}", name="showEntreprise")
     * @param Entreprise $entreprise
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showEntreprise(Entreprise $entreprise)
    {
        return $this->render('entreprise/show.html.twig', [
            'entreprise' => $entreprise,
        ]);
    }

}