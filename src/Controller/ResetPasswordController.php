<?php

namespace App\Controller;

use App\Form\ResetAdminPasswordType;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use App\Form\ResetPasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;

use App\Entity\Utilisateur;

class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/reset_password", name="reset_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $form = $this->createForm(ResetPasswordType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // Si l'ancien mot de passe est bon
            if ($form->isValid()) {
                $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

                $em->persist($user);
                $em->flush();

                $this->addFlash('notice', 'Votre mot de passe à bien été changé !');

                return $this->redirectToRoute('index');
            } else {
                $form->addError(new FormError('Ancien mot de passe incorrect'));
            }
        }

        return $this->render('reset_password/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/admin/app/eleve/{object}/reset", name="resetPasswordEleve")
     * @param Request $request
     * @param $object
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetElevePassword(Request $request , $object , UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->find($object);
        $form = $this->createForm(ResetAdminPasswordType::class, $user);

        $form->handleRequest($request);
        $generator = new ComputerPasswordGenerator();
        $generator
            ->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols(false)
            ->setLength(10);
        $password = $generator->generatePassword();
        $this->addFlash('user', $user->getUsername());
        if ($form->isSubmitted() && $form->isValid()) {
                $user->setPassword($encoder->encodePassword($user, $password));

                $em->persist($user);
                $em->flush();
                $flashBag = $this->get('session')->getFlashBag();
                $flashBag->get('user');
                $this->addFlash('notice', $password);
        }

        return $this->render('reset_password/adminReset.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/app/admin/{object}/reset", name="resetPasswordAdmin")
     * @param Request $request
     * @param $object
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetAdminPassword(Request $request , $object , UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->find($object);
        $form = $this->createForm(ResetAdminPasswordType::class, $user);

        $form->handleRequest($request);
        $generator = new ComputerPasswordGenerator();
        $generator
            ->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols(false)
            ->setLength(10);
        $password = $generator->generatePassword();
        $this->addFlash('user', $user->getUsername());
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $password));

            $em->persist($user);
            $em->flush();
            $flashBag = $this->get('session')->getFlashBag();
            $flashBag->get('user');
            $this->addFlash('notice', $password);
        }

        return $this->render('reset_password/adminReset.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
