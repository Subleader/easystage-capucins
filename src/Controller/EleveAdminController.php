<?php

namespace App\Controller;

use App\Admin\ElevePDF;
use App\Entity\Utilisateur;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

class EleveAdminController extends CRUDController
{
    /**
     * @param ProxyQueryInterface $selectedModelQuery
     *
     * @return RedirectResponse
     */
    public function batchActionMerge(ProxyQueryInterface $selectedModelQuery)
    {
        $encoder = new BCryptPasswordEncoder(13);
        $this->admin->checkAccess('edit');
        $this->admin->checkAccess('delete');

        $modelManager = $this->admin->getModelManager();
        $selectedModels = $selectedModelQuery->execute();

        // do the merge work here

        try {
            $em = $this->getDoctrine()->getManager();
            $pdf = new ElevePDF();
            $headerTab = ['Utilisateur', 'Login', 'Mot de passe'];
            $eleves = array();
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->AddPage();
            $pdf->EleveHeader();

            foreach ($selectedModels as $selectedModel) {
                $user = $this->getDoctrine()
                    ->getRepository(Utilisateur::class)
                    ->find($selectedModel);
                $generator = new ComputerPasswordGenerator();
                $generator
                    ->setUppercase()
                    ->setLowercase()
                    ->setNumbers()
                    ->setSymbols(false)
                    ->setLength(10);
                $password = $generator->generatePassword();
                $user->setPassword($password);
                $userPDF = clone $user;
                $eleves[] = $userPDF;
                $user->setPassword($encoder->encodePassword($password, ''));

                $em->persist($user);
                $em->flush();
            }

            $pdf->BasicTableEleve($headerTab, $eleves);
            $pdf->Output("F","ListeDesMotsDePasse.pdf",true );
            $modelManager->update($selectedModel);
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', 'flash_batch_merge_error');

            return new RedirectResponse(
                $this->admin->generateUrl('list', [
                    'filter' => $this->admin->getFilterParameters()
                ])
            );
        }

        return new RedirectResponse(
            $this->admin->generateUrl('downpass', [
                'filter' => $this->admin->getFilterParameters()
            ])
        );
    }
}


