<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;

class DownloadPassController extends AbstractController
{
    /**
     * @Route("/admin/app/eleve/downpass", name="download_pass_eleve")
     */
    public function downPass()
    {
        $passpath = "ListeDesMotsDePasse.pdf";
        $file_exists = file_exists($passpath);
        if ($file_exists){
            $response = new BinaryFileResponse($passpath);
            $response->headers->set('Content-Type', 'application/pdf');
            $response->headers->set('Content-Disposition', 'attachment; filename="Liste_mot_de_passe_' . date('Ymd_His') . '.pdf"');
            $response->deleteFileAfterSend(true);
            $response->send();
            return $response;
        } else {
            return $this->render(
                'download_pass/index.html.twig',
                array( 'file_exists' => $file_exists )
            );
        }
    }

    /**
     * @Route("/admin/app/eleve/{object}/downpass", name="download_pass_single")
     */
    public function downPassSingle()
    {
        $passpath = "ListeDesMotsDePasse.pdf";
        $file_exists = file_exists($passpath);
        if ($file_exists){
            $response = new BinaryFileResponse($passpath);
            $response->headers->set('Content-Type', 'application/pdf');
            $response->headers->set('Content-Disposition', 'attachment; filename="Liste_mot_de_passe_' . date('Ymd_His') . '.pdf"');
            $response->deleteFileAfterSend(true);
            $response->send();
            return $response;
        } else {
            return $this->render(
                'download_pass/index.html.twig',
                array( 'file_exists' => $file_exists )
            );
        }
    }
    /**
     * @Route("/admin/app/admin/downpass", name="download_pass_admin")
     */
    public function downPassAdmin()
    {
        $passpath = "ListeDesMotsDePasse.pdf";
        $file_exists = file_exists($passpath);
        if ($file_exists){
            $response = new BinaryFileResponse($passpath);
            $response->headers->set('Content-Type', 'application/pdf');
            $response->headers->set('Content-Disposition', 'attachment; filename="Liste_mot_de_passe_' . date('Ymd_His') . '.pdf"');
            $response->deleteFileAfterSend(true);
            $response->send();
            return $response;
        } else {
            return $this->render(
                'download_pass/index.html.twig',
                array( 'file_exists' => $file_exists )
            );
        }
    }
}
