<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Entity\Division;
use App\Entity\Eleve;
use App\Entity\Entreprise;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login/index.html.twig', [
            'controller_name' => 'LoginController',
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/newuser", name="newUser")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newUser(Request $request)
    {
        $formType = $this->createFormBuilder()
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    'Eleve' => 'Eleve',
                    'Admin' => 'Admin'
                )))
            ->getForm();

        $formType->handleRequest($request);
        if ($formType->isSubmitted() && $formType->isValid()) {
            $type = $formType->getData()['type'];

            return $this->redirectToRoute('newUserType', array('type' => $type));
        }

        return $this->render('login/newuser.html.twig', [
            'form' => $formType->createView(),
            'type' => 'Utilisateur'
        ]);
    }

    /**
     * @Route("/newuser/{type}", name="newUserType")
     * @param String $type
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */


    public function newUserForm(String $type, Request $request, UserPasswordEncoderInterface $encoder)
    {

        $formBuild = null;

        if ($type == 'Eleve') {
            $formBuild = $this->createFormBuilder(new Eleve());
        } elseif ($type == 'Admin') {
            $formBuild = $this->createFormBuilder(new Admin())
                ->add('email', EmailType::class);
        }

        $formBuild->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('login', TextType::class)
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password')
            ));

        $form = $formBuild->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('login/newuser.html.twig', [
            'type' => $type,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/AddEnGros/{type}", name="AddUserEnGros")
     * @param UserPasswordEncoderInterface $encoder
     * @param String $type
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function newUserEnGros(UserPasswordEncoderInterface $encoder, String $type)
    {
        $type = strtolower($type);
        $tabNom = ['Jean', 'Paul', 'Levi', 'Guy', 'Catherine', 'Capucine', 'Pierpont', 'Pierre', 'Virginie', 'Fleur', 'Colette', 'Amaury', 'Jeanne', 'Gilles', 'Ancelote', 'William'];
        $tabPrenom = ['Dias', 'Huff', 'Gouddreau', 'Gilbert', 'Leroux', 'Audet', 'Cailot', 'Courtemanche', 'Blanc', 'Houde', 'Reault', 'Dubé', 'Bizier', 'Aubin', 'Chassé'];

        $division = new Division();
        $division->setNom("3 1");
        $division->setCommentaire("For Test");
        $division->setAnnee("1998");

        $eleve = new Eleve();
        $eleve->setNom('Jean');
        $eleve->setPrenom('Jean');
        $eleve->setLogin('Jean' . '123');
        $eleve->setDivision($division);
        $eleve->setPassword($encoder->encodePassword($eleve, '123'));
        $this->getDoctrine()->getManager()->persist($eleve);


        $entreprise = new Entreprise();
        $entreprise->setNom('Jean S. A. S.');
        $entreprise->setAdresse(14 . 'rue des' . 'Jean');
        $entreprise->setCodePostal('77000');
        $entreprise->setEmail('entreprise@entreprise.com');
        $entreprise->setSecteur('Informatique');
        $entreprise->setTelephone('0606060606');
        $entreprise->setVille('Melun');
        $this->getDoctrine()->getManager()->persist($entreprise);

        $this->getDoctrine()->getManager()->flush();

        for ($i = 0; $i < 50; $i++) {

            if ($type == 'eleve') {
                $user = new Eleve();

                $ranNom = random_int(0, count($tabNom) - 1);
                $ranPrenom = random_int(0, count($tabPrenom) - 1);

                $user->setNom($tabNom[$ranNom]);
                $user->setPrenom($tabPrenom[$ranPrenom]);
                $user->setLogin($tabPrenom[$ranPrenom] . $ranNom[$ranPrenom] . '123');

                if ($type == 'eleve' || $type == 'Eleve')

                    $user->setDivision($division);

                $user->setPassword($encoder->encodePassword($user, '123'));
                $this->getDoctrine()->getManager()->persist($user);

            } elseif ($type == 'entreprise' || $type == 'ent') {
                $entreprise = new Entreprise();
                $entreprise->setNom($tabNom[random_int(0, count($tabNom) - 1)] . 'S. A. S.');
                $entreprise->setAdresse(random_int(0, 99) . 'rue des' . $tabPrenom[random_int(0, count($tabPrenom) - 1)]);
                $entreprise->setCodePostal('77000');
                $entreprise->setEmail('entreprise@entreprise.com');
                $entreprise->setSecteur('Informatique');
                $entreprise->setTelephone('0606060606');
                $entreprise->setVille('Melun');

                $this->getDoctrine()->getManager()->persist($entreprise);
            }

        }

        $this->getDoctrine()->getManager()->flush();

        return $this->render('index/index.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }

    /**
     * @Route("/forgotPassword/", name="forgotPassword")
     */
    public function forgotPassword(\Swift_Mailer $mailer, Request $request){
        $form = $this->createFormBuilder()
            ->add('Email', EmailType::class, [
                'label' => 'Email',
                'attr' => [
                    'class' => 'align-middle',
                    'maxlength' => 128,
                    'style' => 'height: 2.5rem; margin-right: 0.5rem; width: 20rem'
                    ],
            ])
            ->add('Valider', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary align-middle',
                    'style' => 'height: 2.5rem; width: 6rem',
                    ]
            ])
            ->getForm();

        $form = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->getData()['Email'];

            $admin = $this->getDoctrine()->getRepository(Admin::class)->findOneBy([
                'email' =>  $email,
            ]);

            if ($admin != null) {
                if (intval(date_diff(new \DateTime(), $admin->getLastDemande())->format('%i')) > 10) {
                    $generator = new ComputerPasswordGenerator();
                    $generator
                        ->setUppercase()
                        ->setLowercase()
                        ->setNumbers()
                        ->setSymbols(false)
                        ->setLength(128);
                    $token = $generator->generatePassword();

                    $admin->setPasswordToken($token);
                    $admin->setLastDemande(new \DateTime());
                    $this->getDoctrine()->getManager()->persist($admin);
                    $this->getDoctrine()->getManager()->flush();

                    if (substr($request->getUri(), -1) != "/")
                        $lien = $request->getUri() . '/'. $token . '/' . $admin->getEmail();
                    else
                        $lien = $request->getUri() . $token . '/' . $admin->getEmail();

                    $message = (new \Swift_Message('Demande de changement du mot de passe administrateur'))
                        ->setFrom(array('noreply-clg-capucins-melun@gmail.com'))
                        ->setTo(array($admin->getEmail()))
                        ->setBody(
                            $this->renderView('reset_password/adminResetByMail.html.twig',
                                ['lien' => $lien]
                            ), 'text/html'
                        );

                    $mailer->send($message);

                    $this->addFlash('warning', 'Email Envoyé');

                    return $this->redirectToRoute('forgotPassword');
                } else {
                    $this->addFlash('warning', 'La dernière demande a eu lieu il y a moins de 10 minutes. Réessayez plus tard.');

                    return $this->redirectToRoute('forgotPassword');
                }
            } else {
                $this->addFlash('warning', 'Adresse mail inexistant');

                return $this->redirectToRoute('forgotPassword');
            }
        }

        return $this->render('login/forgotPassword.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/forgotPassword/{token}/{email}", name="resetAdminPasswordByToken")
     */
    public function resetAdminPasswordByToken(String $token, String $email, Request $request, UserPasswordEncoderInterface $encoder){

        $form = $this->createFormBuilder()
            ->add('password', RepeatedType::class, [
                    'first_options'  => [
                        'label' => ' ',
                        'attr'=> array(
                            'placeholder' => "Nouveau Mot de passe",
                            'class' => 'input100',
                            'type' => "password",
                        )],
                    'second_options'  => ['label' => ' ',
                        'attr'=> array(
                            'placeholder' => "Retapez Mot de passe",
                            'class' => 'input100',
                            'type' => "password",
                        )],
                    'type' => PasswordType::class,
                    'invalid_message' => 'Les deux mots de passe doivent être identiques',
                    'required' => true,
            ])
            ->getForm();

        $form = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $admin = $this->getDoctrine()->getRepository(Admin::class)->findOneBy([
                'email' =>  $email,
            ]);

            if ($admin != null) {
                if (intval(date_diff(new \DateTime(), $admin->getLastDemande())->format('%i')) < 10) {
                    if ($token == $admin->getPasswordToken()){
                        $encodedPassword = $encoder->encodePassword($admin, $form->getData()['password']);

                        $admin->setPassword($encodedPassword);
                        $admin->setPasswordToken('null');
                        $admin->setLastDemande(new \DateTime());

                        $this->getDoctrine()->getManager()->persist($admin);
                        $this->getDoctrine()->getManager()->flush();
                        $this->addFlash('success', 'Mot de passe changé avec succès');
                    } else {
                        $this->addFlash('warning', 'Token invalide');

                        return $this->redirectToRoute('index');
                    }

                } else {
                    $this->addFlash('warning', 'La demande a eu lieu il y a plus de 10 minutes ou lien expiré');

                    return $this->redirectToRoute('index');
                }
            }else {
                $this->addFlash('warning', 'Adresse mail inexistant');

                return $this->redirectToRoute('index');
            }
        }


        return $this->render('reset_password/newAdminPasswordFromMail.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
