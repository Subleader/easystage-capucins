<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190115065748 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise CHANGE telephone telephone VARCHAR(12) DEFAULT NULL, CHANGE secteur secteur VARCHAR(255) NOT NULL, CHANGE codepostal code_postal VARCHAR(10) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE entreprise CHANGE secteur secteur VARCHAR(15) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE telephone telephone VARCHAR(15) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE code_postal codepostal VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
