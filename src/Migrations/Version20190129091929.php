<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190129091929 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stage DROP INDEX UNIQ_C27C9369A6CC7B2, ADD INDEX IDX_C27C9369A6CC7B2 (eleve_id)');
        $this->addSql('ALTER TABLE stage CHANGE eleve_id eleve_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stage DROP INDEX IDX_C27C9369A6CC7B2, ADD UNIQUE INDEX UNIQ_C27C9369A6CC7B2 (eleve_id)');
        $this->addSql('ALTER TABLE stage CHANGE eleve_id eleve_id INT DEFAULT NULL');
    }
}
