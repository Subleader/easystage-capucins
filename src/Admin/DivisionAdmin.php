<?php

namespace App\Admin;

use App\Entity\Division;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DivisionAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Division
            ? $object->getNom()
            : ' Division';
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $admin = $this;

        $formMapper->getFormBuilder()->addEventListener(FormEvents::SUBMIT,
            function (FormEvent $event) use ($formMapper, $admin) {
                $form = $event->getForm();

                $dataDivision = $form->getData();

                $event->setData($dataDivision);
            });

        $formMapper
            ->add('nom', TextType::class, [
                'attr' => ['maxlength' => 5]
            ])
            ->add('commentaire', TextType::class, [
                'attr' => ['maxlength' => 255],
                'required' => false,
            ])
            ->add('annee', TextType::class, [
                'attr' => [
                    'class' => 'js-datepicker datepicker1',
                    'maxlength' => 9
                    ]
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('commentaire')
            ->add('annee');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->addIdentifier('nom')
            ->add('commentaire')
            ->add('annee');
    }

    public function getExportFormats()
    {
        return array();
    }
}
