<?php

namespace App\Admin;

use App\Entity\Division;
use App\Entity\Eleve;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FPDF;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use function League\Csv\delimiter_detect;
use League\Csv\Exception;
use League\Csv\RFC4180Field;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\Form\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use League\Csv\Reader;
use League\Csv\Statement;

class EleveAdmin extends AbstractAdmin
{

    public function postPersist($object)
    {
        $this->getRequest()->getSession()->getFlashBag()->add("warning", "<a href=\"downpass\"> Télécharger les mots de passe (Lien temporaire)</a>");
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $this->getRequest()->getSession()->getFlashBag()->get('danger');
        if ($errorElement->getErrors()) {
            $this->getRequest()->getSession()->getFlashBag()->add("danger", "Une erreur est survenue, il est probable que vous ayez entré un login déjà existant");
        }
    }

    public function toString($object)
    {
        return $object instanceof Eleve
            ? $object->getLogin()
            : ' Eleve';
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        return $list;
    }

    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        return $actions;
    }

    public function getBatchActions()
    {
        // retrieve the default (currently only the delete action) actions
        $actions = parent::getBatchActions();

        // check user permissions
        if ($this->hasRoute('edit') && $this->isGranted('EDIT') && $this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['merge'] = [
                'label' => $this->trans('Réinitialiser mot de passe', array(), 'SonataAdminBundle'),
                'ask_confirmation' => true // If true, a confirmation will be asked before performing the action
            ];

        }

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {

        $collection->add('downpass');
        $collection->add('reset', $this->getRouterIdParameter() . '/reset');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $admin = $this;

        if ($this->isCurrentRoute('create')) {

            $formMapper->getFormBuilder()->addEventListener(FormEvents::SUBMIT,
                function (FormEvent $event) use ($formMapper, $admin) {

                    $form = $event->getForm();
                    $file = $form['file']->getData();
                    $data = $form->getData();
                    $encoder = new BCryptPasswordEncoder(13);
                    $redir = true;
                    if (($data->getNom() != '-1' and $data->getPrenom() != '-1'
                        and $data->getPassword() != '-1' and $data->getLogin() != '-1')) {


                        $password = $this->generateRandomPassword();
                        $redir = false;


                        $pdf = new ElevePDF();
                        $headerTab = ['Utilisateur', 'Login', 'Mot de passe'];
                        $data = $data->setPassword($password);
                        $eleves = [$data];
                        $pdf->SetFont('Arial', 'B', 15);
                        $pdf->AddPage();
                        $pdf->EleveHeader();
                        $pdf->BasicTableEleve($headerTab, $eleves);
                        $pdf->Output("F", "ListeDesMotsDePasse.pdf", true);

                        $data = $data->setPassword($encoder->encodePassword($password, ''));
                        $event->setData($data);
                    }
                    if ($file != null) {
                        $this->doImport($form, $encoder);

                        if ($redir) {
                            $redirection = new RedirectResponse($this->getConfigurationPool()->getContainer()->get('router')->generate('admin_app_eleve_list'));

                            $redirection->send();
                        }
                    }
                });

            $formMapper
                ->tab('Ajout Unique')
                ->add('nom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('prenom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('login', TextType::class, [
                    'attr' => ['maxlength' => 128]
                ])
                ->add('division', ModelListType::class, [
                    'class' => Division::class,
                    'allow_extra_fields' => true,
                ])
                ->end()->end()
                ->tab('Importation CSV')
                ->add('file', FileType::class, [
                    'mapped' => false,
                    'required' => false,
                    'attr' => ['onchange' => 'changeText()', 'class' => 'hidden'],
                    'label_attr' => ['id' => 'label_form', 'class' => 'btn btn-info label_file'],

                ])
                ->add('annee', TextType::class, array(
                    'mapped' => false,
                    'required' => false,
                    'attr' => ['class' => 'js-datepicker datepicker1',],
                    'label' => 'Année scolaire'))
                ->end();
        } else if ($this->isCurrentRoute('edit')) {
            $formMapper
                ->add('nom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('prenom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('login', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('division', ModelListType::class, [
                    'class' => Division::class,

                    'allow_extra_fields' => true,
                ]);
        }
    }

    public function doImport(FormInterface $form, BCryptPasswordEncoder $encoder)
    {
        $listLogin = [];
        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $newFilename = 'list.csv';
        if (file_exists($newFilename)) {
            unlink($newFilename) or die ("n'existe pas");
        }

        $pdf = new ElevePDF();
        $headerTab = ['Utilisateur', 'Login', 'Mot de passe'];
        $eleves = array();
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->AddPage();
        $pdf->EleveHeader();

        $file = new UploadedFile($form['file']->getData(), $newFilename);
        $file->move('./', $newFilename);

        $csv = Reader::createFromPath($newFilename, 'r');
        RFC4180Field::addTo($csv);
        $result = delimiter_detect($csv, [';', '|', ','], -1);

        try {
            $csv->setDelimiter('z');
            foreach ($result as $delim => $nbUse) {
                if ($csv->getDelimiter() == 'z') {
                    $csv->setDelimiter($delim);
                } elseif ($result[$csv->getDelimiter()] < $nbUse) {
                    $csv->setDelimiter($delim);
                }
            }
        } catch (Exception $e) {
            exit("setDelimiter() a crash");
        }
        $csv->setOutputBOM(Reader::BOM_UTF8);


        try {
            $stmt = (new Statement())
                ->offset(1);
        } catch (Exception $e) {
            exit("new Statement())->offset() a crash");
        }


        $records = $stmt->process($csv);
        ini_set('max_execution_time', 0);

        $password = null;

        foreach ($records as $offset => $record) {
            if (isset($record[4])){
                $this->getRequest()->getSession()->getFlashBag()->add("warning", "Ce fichier ne correspond pas aux attentes");
                $redirection = new RedirectResponse($this->getConfigurationPool()->getContainer()->get('router')->generate('admin_app_eleve_create'));
                $redirection->send();
            }
            if (!isset($record[3])){
                $this->getRequest()->getSession()->getFlashBag()->add("warning", "Ce fichier ne correspond pas aux attentes");
                $redirection = new RedirectResponse($this->getConfigurationPool()->getContainer()->get('router')->generate('admin_app_eleve_create'));
                $redirection->send();
            }
            $nom = utf8_encode((string)$record[0]);

            $prenom = utf8_encode((string)$record[1]);

            $nomDivision = utf8_encode((string)$record[2]);

            $situation = utf8_encode((string)$record[3]);


            $division = $em->getRepository(Division::class)->findOneBy(['nom' => $nomDivision, 'annee' => $form['annee']->getData()]);
            if ($division == null) {
                $division = new Division();
                $division->setNom($record[2]);
                $division->setCommentaire(' ');


                $division->setAnnee($form['annee']->getData());
                try {
                    $em->persist($division);
                } catch (ORMException $e) {
                }
                try {
                    $em->flush();
                } catch (OptimisticLockException $e) {
                } catch (ORMException $e) {
                }
                $division = $em->getRepository(Division::class)->findOneBy(['nom' => $record[2], 'annee' => $form['annee']->getData()]);
            }


            if (strpos($situation, 'Non') != 'false') {


                $eleve = $em->getRepository(Eleve::class)->findOneBy(['nom' => $nom, 'prenom' => $prenom]);
                if ($eleve == null) {

                    $password = $this->generateRandomPassword();
                    $eleve = new Eleve();
                    $eleve->setNom($nom);
                    $eleve->setPrenom($prenom);
                    $eleve->setDivision($division);
                    $login = mb_substr($nom, 0, 3, 'UTF-8') . mb_substr($prenom, 0, 3, 'UTF-8');
                    $tmpLogin = $login;

                    $index = 0;
                    while ($em->getRepository(Eleve::class)->findOneBy(['login' => $login]) == null and in_array($login, $listLogin)) {
                        $index++;
                        $login = $tmpLogin . $index;
                    }
                    $listLogin[] = $login;
                    $eleve->setLogin($login);
                    $eleve->setPassword($password);

                    $elevePDF = clone $eleve;
                    $eleves[] = $elevePDF;

                    $eleve->setPassword($encoder->encodePassword($eleve, $password));

                    try {
                        $em->persist($eleve);


                    } catch (ORMException $e) {

                    }

                }
            }

        }

        try {
            $em->flush();
        } catch (OptimisticLockException $e) {

        } catch (ORMException $e) {

        }

        $this->getRequest()->getSession()->getFlashBag()->clear();
        $this->getRequest()->getSession()->getFlashBag()->add("warning", "<a href=\"downpass\"> Télécharger les mots de passe (Lien temporaire)</a>");
        $pdf->BasicTableEleve($headerTab, $eleves);
        $pdf->Output("F", "ListeDesMotsDePasse.pdf", true);
        unlink($newFilename) or die("Erreur dela supréssion de " . $newFilename);
        ini_set('max_execution_time', 30);
        return;
    }

    protected function generateRandomPassword()
    {
        $generator = new ComputerPasswordGenerator();
        $generator
            ->setUppercase()
            ->setLowercase()
            ->setNumbers()
            ->setSymbols(false)
            ->setLength(10);
        return ($generator->generatePassword());
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('prenom')
            ->add('login')
            ->add('division');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->addIdentifier('nom')
            ->add('prenom')
            ->add('login')
            ->add('division')
            ->add('_action', null, [
                'actions' => [
                    'delete' => [],
                    'reset' => [
                        'template' => 'sonata/eleve/resetEleveButton.html.twig'
                    ]]
            ]);
    }

    public function getExportFormats()
    {
        return array();
    }
}

class ElevePDF extends FPDF
{

    /**
     * @param $headerTab array() header du tableau par exemple ['Utilisateur', 'Login', 'Mot de passe']
     * @param array $eleves array(Eleve) Les eleves à ajouter au tableau par exemple [Eleve1, Eleve2]
     */
    function BasicTableEleve($headerTab, array $eleves)
    {
        // En-tête
        $this->Cell(90, 10, $headerTab[0], 1);
        $this->Cell(45, 10, $headerTab[1], 1);
        $this->Cell(45, 10, $headerTab[2], 1);

        $this->Ln();
        // Données

        $divisions = array();

        foreach ($eleves as $eleve) {
            $divisions[$eleve->getDivision()->toString()][] = $eleve;
        }

        ksort($divisions);

        foreach ($divisions as $division => $eleves) {
            $this->Cell(180, 8, $division, 1, 0, 'C');
            $this->Ln();

            foreach ($eleves as $eleve) {
                $this->Cell(90, 8, utf8_decode(mb_substr($eleve->getNom(), 0, 18, 'UTF-8')) . " " . utf8_decode(mb_substr($eleve->getPrenom(), 0, 10, 'UTF-8')), 1);
                $this->Cell(45, 8, utf8_decode($eleve->getLogin()), 1);
                $this->Cell(45, 8, utf8_decode($eleve->getPassword()), 1);
                $this->Ln();
            }
        };
        $this->Ln();
    }

    /**
     *  Header/Titre sur le document
     */
    function EleveHeader()
    {
        $this->ln();
        $this->Cell(60);
        $this->Cell(65, 10, 'Liste des mots de passe - ' . date('Y/m/d H:i:s'), 0, 0, 'C');
        $this->ln(20);
    }
}