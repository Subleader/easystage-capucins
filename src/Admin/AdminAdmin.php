<?php

namespace App\Admin;


use App\Entity\Admin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\Form\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

class AdminAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Admin
            ? $object->getNom()
            : ' Admin';
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $this->getRequest()->getSession()->getFlashBag()->clear();
        if ($errorElement->getErrors()){
            $this->getRequest()->getSession()->getFlashBag()->add("danger", "Une erreur est survenue, il est probable que vous ayez entré un login ou un email déjà existant");
        }
    }

    public function getBatchActions()
    {
        // retrieve the default (currently only the delete action) actions
        $actions = parent::getBatchActions();

        // check user permissions
        if ($this->hasRoute('edit') && $this->isGranted('EDIT') && $this->hasRoute('delete') && $this->isGranted('DELETE')) {
            $actions['merge'] = [
                'label' => $this->trans('Réinitialiser mot de passe', array(), 'SonataAdminBundle'),
                'ask_confirmation' => true // If true, a confirmation will be asked before performing the action
            ];

        }

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {

        $collection->add('downpass');
        $collection->add('reset', $this->getRouterIdParameter() . '/reset');
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $admin = $this;
        if ($this->isCurrentRoute('create')) {
            $formMapper->getFormBuilder()->addEventListener(FormEvents::SUBMIT,
                function (FormEvent $event) use ($formMapper, $admin) {
                    $encoder = new BCryptPasswordEncoder(13);
                    $form = $event->getForm();

                    $dataUserAdmin = $form->getData();
                    $dataUserAdmin = $dataUserAdmin->setPassword($encoder->encodePassword($dataUserAdmin->getPassword(), ''));

                    $event->setData($dataUserAdmin);
                });

            $formMapper
                ->add('nom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('prenom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('email', EmailType::class, [
                    'attr' => ['maxlength' => 128]
                ])
                ->add('login', TextType::class, [
                    'attr' => ['maxlength' => 128]
                ])
                ->add('password', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => 'The password fields must match.',
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => false,
                    'first_options' => ['label' => 'Nouveau mot de passe'],
                    'second_options' => ['label' => 'Répétez le mot de passe'],
                    'attr' => ['maxlength' => 255]
                ]);
        }else if ($this->isCurrentRoute('edit')){
            $formMapper
                ->add('nom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('prenom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('email', EmailType::class, [
                    'attr' => ['maxlength' => 128]
                ])
                ->add('login', TextType::class, [
                    'attr' => ['maxlength' => 128]
                ]);
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('prenom')
            ->add('login')
            ->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->addIdentifier('nom')
            ->add('prenom')
            ->add('login')
            ->add('email')
            ->add('_action', null, [
                'actions' => [
                    'delete' => [],
                    'reset' => [
                        'template' => 'sonata/eleve/resetEleveButton.html.twig'
                    ]]
            ]);
    }

    public function getExportFormats()
    {
        return array();
    }
}