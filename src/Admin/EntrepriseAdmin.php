<?php

namespace App\Admin;


use App\Entity\Entreprise;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use function League\Csv\delimiter_detect;
use League\Csv\Exception;
use League\Csv\RFC4180Field;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use League\Csv\Reader;
use League\Csv\Statement;


class EntrepriseAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $admin = $this;

        if ($this->isCurrentRoute('create')) {

            $formMapper->getFormBuilder()->addEventListener(FormEvents::SUBMIT,
                function (FormEvent $event) use ($formMapper, $admin) {

                    $form = $event->getForm();
                    $file = $form['file']->getData();
                    $data = $form->getData();
                    $redir = true;
                    if (($data->getNom() != '-1' and $data->getVille() != '-1')) {

                        $redir = false;


                        $event->setData($data);

                    }
                    if ($file != null) {
                        $this->doImport($form);

                        if ($redir) {
                            $redirection = new RedirectResponse($this->getConfigurationPool()->getContainer()->get('router')->generate('admin_app_entreprise_list'));

                            $redirection->send();
                        }
                    }
                });

            $formMapper
                ->tab('Ajout Unique')
                ->add('nom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('adresse', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('ville', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('codePostal', TextType::class, [
                    'attr' => ['maxlength' => 10]
                ])
                ->add('email', TextType::class, [
                    'attr' => ['maxlength' => 255],
                    'required' => false,
                ])
                ->add('telephone', TextType::class, [
                    'attr' => ['maxlength' => 12],
                    'required' => false,
                ])
                ->add('secteur', ChoiceType::class, array(
                    'choices' => array(
                        'Enseignement' => 'Enseignement',
                        'Commerce/Distribution' => 'Commerce/Distribution',
                        'Hôtellerie/Restauration' => 'Hôtellerie/Restauration',
                        'Droit/Justice' => 'Droit/Justice',
                        'Multimedia/Audiovisuel/Informatique' => 'Multimedia/Audiovisuel/Informatique',
                        'Sciences humaines' => 'Sciences    humaines',
                        'Industrie' => 'Industrie',
                        'Transports/Automobile' => 'Transports/Automobile',
                        'Bâtiment/Construction' => 'Bâtiment/Construction',
                        'Banque/Assurance' => 'Banque/Assurance',
                        'Logistique' => 'Logistique',
                        'Fonction publique' => 'Fonction publique',
                        'Santé' => 'Santé',
                        'Art/Spectacle' => 'Art/Spectacle',
                        'Sport' => 'Sport',
                        'Autre' => 'Autre',),
                    'label' => 'secteur',
                    'attr' => array(
                        'class' => 'input100 custom-select',)
                ))
                ->add('affichage', ChoiceType::class, [
                    'choices' => [
                        'Oui' => '1',
                        'Non' => '0'
                    ]
                ])->end()->end()
                ->tab('Importation CSV')
                ->add('file', FileType::class, [
                    'mapped' => false,
                    'required' => false,
                    'attr' => ['onchange' => 'changeText()', 'class' => 'hidden'],
                    'label_attr' => ['id' => 'label_form', 'class' => 'btn btn-info label_file'],

                ])->end();
        } else if ($this->isCurrentRoute('edit')) {
            $formMapper
                ->add('nom', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('adresse', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('ville', TextType::class, [
                    'attr' => ['maxlength' => 255]
                ])
                ->add('codePostal', TextType::class, [
                    'attr' => ['maxlength' => 10]
                ])
                ->add('email', TextType::class, [
                    'attr' => ['maxlength' => 255],
                    'required' => false,
                ])
                ->add('telephone', TextType::class, [
                    'attr' => ['maxlength' => 12],
                    'required' => false,
                ])
                ->add('secteur', ChoiceType::class, array(
                    'choices' => array(
                        'Enseignement' => 'Enseignement',
                        'Commerce/Distribution' => 'Commerce/Distribution',
                        'Hôtellerie/Restauration' => 'Hôtellerie/Restauration',
                        'Droit/Justice' => 'Droit/Justice',
                        'Multimedia/Audiovisuel/Informatique' => 'Multimedia/Audiovisuel/Informatique',
                        'Sciences humaines' => 'Sciences    humaines',
                        'Industrie' => 'Industrie',
                        'Transports/Automobile' => 'Transports/Automobile',
                        'Bâtiment/Construction' => 'Bâtiment/Construction',
                        'Banque/Assurance' => 'Banque/Assurance',
                        'Logistique' => 'Logistique',
                        'Fonction publique' => 'Fonction publique',
                        'Santé' => 'Santé',
                        'Art/Spectacle' => 'Art/Spectacle',
                        'Sport' => 'Sport',
                        'Autre' => 'Autre',),
                    'label' => 'secteur',
                    'attr' => array(
                        'class' => 'input100 custom-select',)
                ))
                ->add('affichage', ChoiceType::class, [
                    'choices' => [
                        'Oui' => '1',
                        'Non' => '0'
                    ]
                ]);
        }
    }

    public function doImport(FormInterface $form)
    {

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $newFilename = 'listE.csv';
        if (file_exists($newFilename)) {
            unlink($newFilename) or die ("n'existe pas");
        }

        $file = new UploadedFile($form['file']->getData(), $newFilename);
        $file->move('./', $newFilename);

        $csv = Reader::createFromPath($newFilename, 'r');
        RFC4180Field::addTo($csv);
        $result = delimiter_detect($csv, [';', '|', ','], -1);

        try {
            $csv->setDelimiter('z');
            foreach ($result as $delim => $nbUse) {
                if ($csv->getDelimiter() == 'z') {
                    $csv->setDelimiter($delim);
                } elseif ($result[$csv->getDelimiter()] < $nbUse) {
                    $csv->setDelimiter($delim);
                }
            }
        } catch (Exception $e) {
            exit("setDelimiter() a crash");
        }
        $csv->setOutputBOM(Reader::BOM_UTF8);


        try {
            $stmt = (new Statement())
                ->offset(1);
        } catch (Exception $e) {
            exit("new Statement())->offset() a crash");
        }


        $records = $stmt->process($csv);
        ini_set('max_execution_time', 0);

        $listEntreprise = [];

        foreach ($records as $offset => $record) {

            if (!isset($record[7])) {
                $this->getRequest()->getSession()->getFlashBag()->add("warning", "Ce fichier ne correspond pas aux attentes");
                $redirection = new RedirectResponse($this->getConfigurationPool()->getContainer()->get('router')->generate('admin_app_entreprise_create'));
                $redirection->send();
            }

            $nom = ((string)$record[1]);

            $ville = ((string)$record[2]);

            $codePostal = ((string)$record[3]);

            $adresse = ((string)$record[4]);

            $email = ((string)$record[5]);

            $tel = str_replace(' ', '', (string)$record[6]);

            $secteur = ((string)$record[7]);


            $entreprise = $em->getRepository(Entreprise::class)->findOneBy(['nom' => $nom, 'ville' => $ville, 'codePostal' => $codePostal,
                'adresse' => $adresse, 'secteur' => $secteur]);

            if ($entreprise == null) {

                $entreprise = new Entreprise();
                $entreprise->setNom($nom);
                $entreprise->setVille($ville);
                $entreprise->setCodePostal($codePostal);
                $entreprise->setAdresse($adresse);
                $entreprise->setEmail($email);
                $entreprise->setTelephone($tel);
                $entreprise->setSecteur($secteur);
                $entreprise->setAffichage(true);

                if (!in_array($entreprise, $listEntreprise)) {
                    $listEntreprise[]=$entreprise;
                    try {
                        $em->persist($entreprise);


                    } catch (ORMException $e) {

                    }
                }


            }

        }

        try {
            $em->flush();
        } catch (OptimisticLockException $e) {

        } catch (ORMException $e) {

        }

        unlink($newFilename) or die("Erreur dela supréssion de " . $newFilename);
        ini_set('max_execution_time', 30);
        return;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('adresse')
            ->add('ville')
            ->add('codePostal')
            ->add('email')
            ->add('telephone')
            ->add('secteur')
            ->add('affichage');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        unset($this->listModes['mosaic']);

        $listMapper
            ->addIdentifier('nom')
            ->add('adresse')
            ->add('ville')
            ->add('codePostal')
            ->add('email')
            ->add('telephone')
            ->add('secteur')
            ->add('affichage', 'boolean', [
                'editable' => 'yes'
            ]);
    }

    public function toString($object)
    {
        return $object instanceof Entreprise
            ? $object->getNom()
            : ' Entreprise';
    }

    public function getExportFormats()
    {
        return ['csv'];
    }

    public function getExportFields()
    {
        return ['nom', 'adresse', 'ville', 'codePostal', 'email', 'telephone', 'secteur', 'affichage'];
    }
}