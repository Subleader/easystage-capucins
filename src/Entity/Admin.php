<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Admin extends Utilisateur
{
    /**
     * @ORM\Column(name="email", length=128, type="string", unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $passwordToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastDemande;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function toString()
    {
        $this->__toString();
    }

    public function __toString()
    {
        return $this->getNom() . ' ' . $this->getPrenom();
    }

    public function getRoles()
    {
        return array('ROLE_ADMIN');
    }

    public function getPasswordToken(): ?string
    {
        return $this->passwordToken;
    }

    public function setPasswordToken(?string $passwordToken): self
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    public function getLastDemande(): ?\DateTimeInterface
    {
        return $this->lastDemande;
    }

    public function setLastDemande(\DateTimeInterface $lastDemande): self
    {
        $this->lastDemande = $lastDemande;

        return $this;
    }
}
