<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EleveRepository")
 */
class Eleve extends Utilisateur
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Division", inversedBy="eleve", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $division;


    public function getRoles()
    {
        return array('ROLE_ELEVE');
    }

    public function toString(){
        return $this->getNom().' '.$this->getPrenom();
    }

    public function getDivision(): ?Division
    {
        return $this->division;
    }

    public function setDivision(Division $division): self
    {
        $this->division = $division;

        return $this;
    }

    public function __toString()
    {
        return $this->toString();
    }
}
